const alphabet = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'];

function generateAlphaCipher(order = 0) {
  let alphabetCifer = alphabet.slice(0)
  const pushed = alphabetCifer.slice(0, order);
  alphabetCifer.splice(0, order);
  return alphabetCifer.concat(pushed);
}

function crypt(plainText, key) {
  const alphabetCesar = generateAlphaCipher(key);
  plainText = plainText.toLowerCase();
  let cipherText = '';
  for (let letter of plainText) {
    let index = alphabet.indexOf(letter);
    cipherText += (alphabetCesar[index]) ? alphabetCesar[index] : letter;
    }
  return cipherText;
}

function decrypt(cipherText, key) {
  const alphabetCesar = generateAlphaCipher(key);
  cipherText = cipherText.toLowerCase()
  let plainText = '';
  for (let letter of cipherText) {
    let index = alphabetCesar.indexOf(letter);
        plainText += (alphabet[index]) ? alphabet[index] : letter;
    }
  return plainText;
}

function decryptBruteForce(cipherText) {
  let resultsText = [];
  cipherText = cipherText.toLowerCase()
  let text = '', alphabetCesar;
  for(let i=1; i<=26; i++){
    plainText = '';
    alphabetCesar = generateAlphaCipher(i);
    for (let letter of cipherText) {
      let index = alphabetCesar.indexOf(letter);
          plainText += (alphabet[index]) ? alphabet[index] : letter;
      }
    resultsText.push(plainText);
  }
  return resultsText;
}

let msg = 'Oi Linux';
let cipherMsg = 'ys vsxeh';
console.log(crypt(msg, 10))
console.log(decrypt(cipherMsg, 10))
console.log(decryptBruteForce(cipherMsg))
